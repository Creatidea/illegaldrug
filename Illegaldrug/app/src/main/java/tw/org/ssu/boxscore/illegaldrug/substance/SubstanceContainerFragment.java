package tw.org.ssu.boxscore.illegaldrug.substance;

import tw.org.ssu.boxscore.illegaldrug.util.ContainerFragment;

/**
 * Created by noel on 2017/4/25.
 */

public class SubstanceContainerFragment extends ContainerFragment {
    // ---------------------------------------------------
    @Override
    public void init() {
        replaceFragment(new SubstanceFragment(), false);
    }
    // ---------------------------------------------------
}
