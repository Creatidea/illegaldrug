package tw.org.ssu.boxscore.illegaldrug.connect;

import android.content.Context;

import com.neilchen.complextoolkit.http.HttpControl;
import com.neilchen.complextoolkit.util.json.JSONParser;

/**
 * Created by noel on 2018/4/12.
 */

public abstract class BaseConnect {
    protected HttpControl httpControl;
    protected JSONParser parser;

    public BaseConnect(Context context) {
        httpControl = new HttpControl(context);
        parser = new JSONParser();
    }
}
