package tw.org.ssu.boxscore.illegaldrug.commodity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.commodity.adapter.CommodityAdapter;
import tw.org.ssu.boxscore.illegaldrug.commodity.model.Commodity;
import tw.org.ssu.boxscore.illegaldrug.detail.DetailFragment;
import tw.org.ssu.boxscore.illegaldrug.util.BaseFragment;
import tw.org.ssu.boxscore.illegaldrug.util.data.DAOUtil;
import tw.org.ssu.boxscore.illegaldrug.util.dialog.LoadingDialog;

/**
 * 西藥藥品商品
 */
public class CommodityFragment extends BaseFragment implements TextWatcher, AdapterView.OnItemClickListener {

    //帶入model做判別顯示中文或英文用的
    public static final int TYPE_ZH = 7777;
    public static final int TYPE_EN = 7778;

    @IntDef({TYPE_ZH, TYPE_EN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LanguageType {

    }

    @BindView(R.id.edit_text)
    EditText editText;
    @BindView(R.id.image_view)
    ImageButton imageView;
    @BindView(R.id.list_view)
    ListView listView;
    //db資料
    private DAOUtil dao;
    //所有資料
    private ArrayList<Commodity> commodities = new ArrayList<>();
    //copy commodities用來給英文查詢用
    private ArrayList<Commodity> commoditiesForEN = new ArrayList<>();
    //用來判斷增加查詢字元或減少查詢字元
    private int enLength = 0;

    //搜尋用 字串
    private String searchDrugName;
    //搜尋的資料
    private ArrayList<Commodity> searchCommodities = new ArrayList<>();
    //搜尋中文時顯示中文 getView要顯示中文
    private CommodityAdapter adapter;

    //判斷使用者輸入為中文還是英文
    private boolean isEnglish;

    @Override
    protected void init() {

        setView(R.layout.fragment_commodity);

        dao = new DAOUtil(getActivity());
        adapter = new CommodityAdapter(mainActivity);

        imageView.setImageResource(R.drawable.icon_search);
        editText.setHint(getString(R.string.commodity_hint));
        listView.setOnItemClickListener(this);
        listView.setAdapter(adapter);
        editText.addTextChangedListener(this);

        new GetAllDatas().execute();
    }

    // ----------------------------------------------------
    @OnClick(R.id.image_view)
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.image_view:
                editText.setText("");
                closeKeyboard();//關閉Keyboard
                enLength = 0;
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        closeKeyboard();

        Bundle bundle = new Bundle();
        Commodity commodity = (Commodity) parent.getAdapter().getItem(position);
        bundle.putParcelable("data", commodity);
        bundle.putInt("detailType", DetailFragment.DETAIL_COMMODITY);
        replaceFragment3(new DetailFragment(), true, bundle);
    }

    // ----------------------------------------------------
    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    // ----------------------------------------------------

    /***
     * start   新字串即將從哪裡加入
     * count   新字串長度
     * before  上一次動作的新字串長度
     * edit    輸入框的信息
     */
    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {

        searchDrugName = text.toString();//取得輸入字串
        isEnglish = searchDrugName.matches("[a-zA-Z\\s\\p{Punct}\\p{ASCII}]*");//判斷中文或英文     英文為true 中文為false  正則表示式

        // 如果輸入字串長度為空
        if (searchDrugName.equals("")) {
            imageView.setImageResource(R.drawable.icon_search);
            //將搜尋的字串長度歸0
            enLength = 0;
            //直接套上既有的所有資料
            adapter.setData(commodities, TYPE_ZH);

        } else {
            imageView.setImageResource(R.drawable.icon_x);
            //撈中文資料
            if (!isEnglish) {
                adapter.setData(getZHDataFromArray(searchDrugName), TYPE_ZH);
            }
            //撈英文資料
            else {
                //使用者加入字串從後面增加
                if ((start + count) == searchDrugName.length()) {
                    adapter.setData(getENDataFromArray(searchDrugName), TYPE_EN);
                }
                //使用者加入字串從前面或中間增加
                else {
                    adapter.setData(reGetENDataFromArray(searchDrugName), TYPE_EN);
                }
            }
        }
    }

    // ----------------------------------------------------

    /**
     * //edit  輸入结束呈现在輸入框中的信息
     *
     * @param text
     */
    @Override
    public void afterTextChanged(Editable text) {

    }

    //-----------------------------------------------

    /**
     * 背景作業，所有的資料
     */
    private class GetAllDatas extends AsyncTask<Void, Void, Void> {

        private LoadingDialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = new LoadingDialog(mainActivity);
            loadingDialog.setLoadingMessage(getString(R.string.text_loading));
            loadingDialog.showLoadingDialog();

        }

        @Override
        protected Void doInBackground(Void... params) {
            //將所有資料裝進去
            commodities = dao.getCommodityData();
            //給英文查詢用
            commoditiesForEN = new ArrayList<>(commodities);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {  //完成撈取資料後展現
            super.onPostExecute(aVoid);
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }

            //一開始先用取中文名稱的adapter  在清單上顯示中文名稱
            adapter.setData(commodities, TYPE_ZH);
        }
    }

    // ----------------------------------------------------

    /**
     * 取得英文名的資料
     *
     * @param keyWord
     * @return
     */
    private ArrayList<Commodity> getENDataFromArray(String keyWord) {

        searchCommodities.clear();
        int sentenceLength = searchDrugName.length();

        if (enLength < sentenceLength && commoditiesForEN.size() > 0) {
            for (int i = 0; i < commoditiesForEN.size(); i++) {
                //比對英文名
                if (commoditiesForEN.get(i).getEngToLowerCase().contains(keyWord.toLowerCase())) {
                    searchCommodities.add(commoditiesForEN.get(i));
                }
            }
        } else {
            //點選x按鈕清空後再進行查詢會因為資料量為0而查無資料的情況
            for (int i = 0; i < commodities.size(); i++) {
                if (commodities.get(i).getEngToLowerCase().contains(keyWord.toLowerCase())) {
                    searchCommodities.add(commodities.get(i));
                }
            }
        }

        enLength = sentenceLength;
        commoditiesForEN.clear();
        commoditiesForEN.addAll(searchCommodities);
        return commoditiesForEN;
    }

    // ----------------------------------------------------

    /**
     * 重新取得英文名資料
     *
     * @param keyWord
     * @return
     */
    private ArrayList<Commodity> reGetENDataFromArray(String keyWord) {

        searchCommodities.clear();
        for (int i = 0; i < commodities.size(); i++) {
            //比對中文名
            if (commodities.get(i).getEngToLowerCase().contains(keyWord.toLowerCase())) {
                searchCommodities.add(commodities.get(i));
            }
        }
        return searchCommodities;
    }

    // ----------------------------------------------------

    /**
     * 取得中文名的資料
     *
     * @param keyWord
     * @return
     */
    private ArrayList<Commodity> getZHDataFromArray(String keyWord) {

        searchCommodities.clear();
        for (int i = 0; i < commodities.size(); i++) {
            //比對中文名
            if (commodities.get(i).getChineseName().contains(keyWord)) {
                searchCommodities.add(commodities.get(i));
            }
        }
        return searchCommodities;
    }
}

