package tw.org.ssu.boxscore.illegaldrug;

import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.commodity.CommodityContainerFragment;
import tw.org.ssu.boxscore.illegaldrug.substance.SubstanceContainerFragment;
import tw.org.ssu.boxscore.illegaldrug.tradition.TraditionContainerFragment;
import tw.org.ssu.boxscore.illegaldrug.util.BaseFragment;

public class MainActivity extends FragmentActivity implements TabHost.OnTabChangeListener {

    public static final int TAB_SUBSTANCE = 0;
    public static final int TAB_COMMODITY = 1;
    public static final int TAB_TRADITION = 2;


    @IntDef({TAB_COMMODITY, TAB_SUBSTANCE, TAB_TRADITION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TargetTab {

    }

    @BindView(android.R.id.tabhost)
    FragmentTabHost tabhost;
    private String[] tabNames;
    private Class[] classes;

    //----------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainhost);
        ButterKnife.bind(this);
        initTabHost();
        selectedTab();
    }

    //----------------------
    @Override
    public void onTabChanged(String tabId) {
        selectedTab();
    }

    //----------------------
    @Override
    public void onBackPressed() {
        backToBeforePage();
    }

    //---------
    @OnClick(R.id.back)
    public void onClicked(View view) {
        backToBeforePage();
    }

    //----------------------
    private void initTabHost() {

        tabNames = getResources().getStringArray(R.array.main_tab_name_array);
        classes = new Class[]{SubstanceContainerFragment.class,CommodityContainerFragment.class,  TraditionContainerFragment.class};
        tabhost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        for (int i = 0; i < classes.length; i++) {
            tabhost.addTab(tabhost.newTabSpec(tabNames[i]).setIndicator(getTabView(tabNames[i], i)), classes[i], null);
        }

        tabhost.setCurrentTab(getIntent().getIntExtra("tab", 0));
        tabhost.setOnTabChangedListener(this);
    }
    //----------------------

    /***
     * 一開始選擇的tab 及 選擇不同tab時都要使用這method
     */
    private void selectedTab() {

        for (int i = 0; i < tabhost.getTabWidget().getTabCount(); i++) {
            TextView textView = (TextView) tabhost.getTabWidget().getChildTabViewAt(i).findViewById(R.id.tab);
            String text = textView.getText().toString();
            textView.setText(getSpannedText(text, text.equals(tabhost.getCurrentTabTag()) ? android.R.color.white : R.color.tab_clicked_subtitle));
        }
    }


    //----------------------

    /***
     * 調整部分文字顏色 或大小
     * @param tabName
     * @return
     */
    private CharSequence getSpannedText(String tabName, int colorRes) {

        SpannableStringBuilder builder = new SpannableStringBuilder(tabName);

        int start = tabName.indexOf("-");
        int end = tabName.lastIndexOf("-") + 1;

        builder.setSpan(new RelativeSizeSpan(1.08f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, colorRes)),start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return builder;
    }


    //----------------------
    //設定tab的背景圖
    private View getTabView(String tabName, int index) {
        View view = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        TextView tab = (TextView) view.findViewById(R.id.tab);
        tab.setText(tabName);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tab.getLayoutParams();

        if (index == tabNames.length - 1) {
            params.leftMargin = 2;
        } else if (index == tabNames.length - 2) {
            params.leftMargin = 1;
            params.rightMargin = 1;
        } else {
            params.rightMargin = 2;
        }
        return view;
    }

    //----------------------

    /**
     * 回到前一頁 如沒有則退出
     */
    private void backToBeforePage() {

        boolean isPopFragment = false;
        String currentTabTag = tabhost.getCurrentTabTag();

        if (isTabTag(currentTabTag)) {
            isPopFragment = ((BaseFragment) getSupportFragmentManager().findFragmentByTag(currentTabTag)).popFragment();
        }

        if (!isPopFragment) {
            this.finish(); // 關閉
        }
    }

    //----------------------

    /**
     * 檢查tag
     */
    private boolean isTabTag(String tabName) {

        for (int i = 0; i < tabNames.length; i++) {
            if (tabName.equals(tabNames[i])) {
                return true;
            }
        }
        return false;
    }

}
