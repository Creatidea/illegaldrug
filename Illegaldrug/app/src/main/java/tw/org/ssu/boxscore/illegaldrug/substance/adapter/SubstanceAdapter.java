package tw.org.ssu.boxscore.illegaldrug.substance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.substance.model.Substance;

/**
 * Created by noel on 2017/3/30.
 */

public class SubstanceAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private LayoutInflater inflater;
    private ArrayList<Substance> substances;


    public SubstanceAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        substances = new ArrayList<>();
    }

    //----------------------

    @Override
    public int getCount() {
        return substances.size();
    }

    //----------------------
    @Override
    public Object getItem(int arg0) {
        return substances.get(arg0);
    }

    //----------------------
    @Override
    public long getItemId(int position) {
        return position;
    }


    //----------------------
    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        TitleViewHolder titleViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_searchtitle, null);
            titleViewHolder = new TitleViewHolder(convertView);
            convertView.setTag(titleViewHolder);
        } else {
            titleViewHolder = (TitleViewHolder) convertView.getTag();
        }
        titleViewHolder.tvTitle.setText(substances.get(position).getCategory());
        return convertView;
    }


    //----------------------
    @Override
    public long getHeaderId(int position) {
        return substances.get(position).getCategory().hashCode();
    }


    //----------------------
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ContentViewHolder contentViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_searchcontent, null);
            contentViewHolder = new ContentViewHolder(convertView);
            convertView.setTag(contentViewHolder);

        } else {
            contentViewHolder = (ContentViewHolder) convertView.getTag();
        }

        Substance substance = (Substance) getItem(position);
        contentViewHolder.tvDrugName.setText(substance.getSubstanceName());

        return convertView;
    }

    //----------------------
    public void setData(ArrayList<Substance> substances) {
        this.substances = substances;
        notifyDataSetChanged();
    }

    //----------------------
    static class ContentViewHolder {
        @BindView(R.id.tv_drug_name)
        TextView tvDrugName;

        ContentViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    //----------------------
    static class TitleViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        TitleViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
