package tw.org.ssu.boxscore.illegaldrug.substance.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by noel on 2017/3/28.
 */

public class Substance implements Parcelable {
    private String substanceName;
    private String category;
    private String substance;
    private String rules;
    private String manual;
    private String note;
    private int sid;


    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getSid() {
        return sid;
    }

    public String getSubstanceName() {
        return substanceName;
    }

    public String getCategory() {
        return category;
    }

    public String getSubstance() {
        return substance;
    }

    public String getRules() {
        return rules;
    }

    public String getManual() {
        return manual;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public void setSubstanceName(String substanceName) {
        this.substanceName = substanceName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubstance(String substance) {
        this.substance = substance;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public void setManual(String manual) {
        this.manual = manual;
    }

    //-----------------

    /***
     * 資料取出
     * @param c
     */
    public void fromCursor(Cursor c) {
        sid = c.getInt(c.getColumnIndex("sid"));
        substanceName = c.getString(c.getColumnIndex("SubstanceName"));
        category = c.getString(c.getColumnIndex("Category"));
        substance = c.getString(c.getColumnIndex("Substance"));
        rules = c.getString(c.getColumnIndex("Rules"));
        manual = c.getString(c.getColumnIndex("Manual"));
        note = c.getString(c.getColumnIndex("Note"));
    }

    //-----------------

    /***
     * 資料存入
     */
    public ContentValues toContentValues() {
        ContentValues ret = new ContentValues();
        ret.put("SubstanceName", getSubstanceName());
        ret.put("Category", getCategory());
        ret.put("Substance", getSubstance());
        ret.put("Rules", getRules());
        ret.put("Manual", getManual());
        ret.put("Note", getNote());
        return ret;
    }
    //--------

    @Override
    public int describeContents() {
        return 0;
    }
    //--------

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.substanceName);
        dest.writeString(this.category);
        dest.writeString(this.substance);
        dest.writeString(this.rules);
        dest.writeString(this.manual);
        dest.writeString(this.note);
        dest.writeInt(this.sid);
    }
    //--------

    public Substance() {
    }
    //--------

    protected Substance(Parcel in) {
        this.substanceName = in.readString();
        this.category = in.readString();
        this.substance = in.readString();
        this.rules = in.readString();
        this.manual = in.readString();
        this.note = in.readString();
        this.sid = in.readInt();
    }
    //--------

    public static final Parcelable.Creator<Substance> CREATOR = new Parcelable.Creator<Substance>() {
        @Override
        public Substance createFromParcel(Parcel source) {
            return new Substance(source);
        }
        //--------

        @Override
        public Substance[] newArray(int size) {
            return new Substance[size];
        }
    };
    //--------

    /***
     * @return detail 的顯示項目
     */
    public String[] toArray() {
        return new String[]{
                getSubstance(),
                getCategory(),
                getRules(),
                getManual(),
                getNote()
        };
    }
}
