package tw.org.ssu.boxscore.illegaldrug.tradition;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.detail.DetailFragment;
import tw.org.ssu.boxscore.illegaldrug.tradition.adapter.TraditionAdapter;
import tw.org.ssu.boxscore.illegaldrug.tradition.model.Tradition;
import tw.org.ssu.boxscore.illegaldrug.util.BaseFragment;
import tw.org.ssu.boxscore.illegaldrug.util.data.DAOUtil;
import tw.org.ssu.boxscore.illegaldrug.util.dialog.LoadingDialog;

/**
 * 中藥
 */

public class TraditionFragment extends BaseFragment implements TextWatcher, AdapterView.OnItemClickListener {

    @BindView(R.id.edit_text)
    EditText editText;
    @BindView(R.id.image_view)
    ImageButton imageView;
    @BindView(R.id.list_view)
    ListView listView;

    private DAOUtil dao;
    //搜尋用
    private ArrayList<Tradition> searchTraditions = new ArrayList<>();
    //所有資料
    private ArrayList<Tradition> traditions = new ArrayList<>();

    private TraditionAdapter traditionAdapter;

    @Override
    protected void init() {

        setView(R.layout.fragment_tradition);

        dao = new DAOUtil(mainActivity);
        traditionAdapter = new TraditionAdapter(mainActivity);
        listView.setOnItemClickListener(this);
        listView.setAdapter(traditionAdapter);
        imageView.setImageResource(R.drawable.icon_search);
        editText.addTextChangedListener(this);
        editText.setHint(getString(R.string.tradition_hint));

        new GetAllDatas().execute();

    }

    //--------------------------------------------------------
    @OnClick(R.id.image_view)
    public void onViewClicked() {
        editText.setText("");
        closeKeyboard();//關閉Keyboard
    }


    //--------------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        closeKeyboard();

        Tradition tradition = (Tradition) parent.getAdapter().getItem(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", tradition);
        bundle.putInt("detailType", DetailFragment.DETAIL_TRADITION);
        replaceFragment3(new DetailFragment(), true, bundle);
    }

    //--------------------------------------------------------
    @Override
    public void beforeTextChanged(CharSequence text, int start, int count, int after) {

    }

    //--------------------------------------------------------

    /***
     * start   新字串即將從哪裡加入
     * count   新字串長度
     * before  上一次動作的新字串長度
     * edit    輸入框的信息
     */
    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {

        String searchDrugName = text.toString();//取得輸入字串

        // 如果輸入字串長度為空
        if (searchDrugName.equals("")) {
            imageView.setImageResource(R.drawable.icon_search);
            //直接套上既有的所有資料
            traditionAdapter.setData(traditions);
        } else {
            imageView.setImageResource(R.drawable.icon_x);
            traditionAdapter.setData(getTraditionDataFromArray(searchDrugName));
        }
    }

    //--------------------------------------------------------

    /**
     * //edit  輸入结束呈现在輸入框中的信息
     *
     * @param text
     */
    @Override
    public void afterTextChanged(Editable text) {

    }


    //--------------------

    /**
     * 背景作業，所有的資料
     */
    private class GetAllDatas extends AsyncTask<Void, Void, Void> {

        private LoadingDialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = new LoadingDialog(mainActivity);
            loadingDialog.setLoadingMessage(getString(R.string.text_loading));
            loadingDialog.showLoadingDialog();

        }

        @Override
        protected Void doInBackground(Void... params) {
            traditions = dao.getTraditionData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {  //完成撈取資料後展現
            super.onPostExecute(aVoid);
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }

            //一開始先用取中文名稱的adapter  在清單上顯示中文名稱
            traditionAdapter.setData(traditions);
        }
    }

    //--------------------------------------------------------

    /**
     * 取得相似名稱的資料
     *
     * @param keyWord
     * @return
     */
    private ArrayList<Tradition> getTraditionDataFromArray(String keyWord) {

        searchTraditions.clear();
        for (int i = 0; i < traditions.size(); i++) {
            //比對中文名
            if (traditions.get(i).getName().contains(keyWord)) {
                searchTraditions.add(traditions.get(i));
            }
        }
        return searchTraditions;
    }
}
