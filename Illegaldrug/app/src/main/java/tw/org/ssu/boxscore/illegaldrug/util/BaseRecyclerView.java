package tw.org.ssu.boxscore.illegaldrug.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by noel on 2018/4/16.
 */

public abstract class BaseRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    protected onItemClickListener onItemClickListener = null;

    //---------

    public interface onItemClickListener {
        void onItemClick(View view, int position);
    }
    //---------

    public void setOnItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
