package tw.org.ssu.boxscore.illegaldrug.tradition.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.tradition.model.Tradition;

/**
 * Created by noel on 2017/3/29.
 */

public class TraditionAdapter extends BaseAdapter {


    private LayoutInflater inflater;
    private ArrayList<Tradition> traditions;

    public TraditionAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        traditions = new ArrayList<>();

    }

    //-----------------------------------------------
    public void setData(ArrayList<Tradition> traditions) {
        this.traditions = traditions;
        notifyDataSetChanged();
    }

    //----------------------
    @Override
    public int getCount() {
        return traditions.size();
    }

    //----------------------
    @Override
    public Object getItem(int arg0) {
        return traditions.get(arg0);
    }

    //----------------------
    @Override
    public long getItemId(int position) {
        return position;
    }

    //----------------------
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_searchcontent, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Tradition tradition = (Tradition) getItem(position);
        holder.tvDrugName.setText(tradition.getName());
        return convertView;
    }

    //----------------------
    class ViewHolder {
        @BindView(R.id.tv_drug_name)
        TextView tvDrugName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
