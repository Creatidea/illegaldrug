package tw.org.ssu.boxscore.illegaldrug.startpage;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CSVDrugType {

    //中藥藥品商品名
    public static final String CSV_TRADITION = "tradition";
    //西藥禁用物質名
    public static final String CSV_SUBSTANCE = "substance";
    //西藥藥品商品名
    public static final String CSV_COMMODITY = "commodity";

    @StringDef({CSV_TRADITION, CSV_SUBSTANCE, CSV_COMMODITY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DrugType {
    }
}
