package tw.org.ssu.boxscore.illegaldrug.about;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.R;

public class AboutActivity extends FragmentActivity {

    @BindView(R.id.view_sportsystem)
    View viewSystem;
    @BindView(R.id.view_service)
    View viewService;
    @BindView(R.id.view_attention)
    View viewAttention;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        initView();
    }

    //  ========================================  ========================================  ========================================

    private void initView() {

        TextView tvServiceTitle = ButterKnife.findById(viewService, R.id.tv_title);
        TextView tvServiceContent = ButterKnife.findById(viewService, R.id.tv_content);
        tvServiceTitle.setText(getString(R.string.about_service_title));
        tvServiceContent.setText(getString(R.string.about_service_content));


        TextView tvSystemTitle = ButterKnife.findById(viewSystem, R.id.tv_title);
        TextView tvSystemContent = ButterKnife.findById(viewSystem, R.id.tv_content);
        tvSystemTitle.setText(getString(R.string.about_sport_system_title));
        tvSystemContent.setText(getString(R.string.about_sport_system_content));

        TextView tvAttentionTitle = ButterKnife.findById(viewAttention, R.id.tv_title);
        TextView tvAttentionContent = ButterKnife.findById(viewAttention, R.id.tv_content);
        tvAttentionTitle.setText(getString(R.string.about_attention_title));
        tvAttentionContent.setText(getString(R.string.about_attention_content));
    }


    //-----------------------------------------------
    @OnClick({R.id.back})
    public void onClicked(View view) {
        finish();
    }
}
