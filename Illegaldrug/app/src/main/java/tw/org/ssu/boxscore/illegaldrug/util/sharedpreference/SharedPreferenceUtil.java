package tw.org.ssu.boxscore.illegaldrug.util.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by winni on 2018/4/13.
 */

public class SharedPreferenceUtil {

    private final String _NAME = "SharedPreferenceUtil";

    //db名稱
    private final String _DB = "DatabasePath";


    //西藥 藥品商品名
    private final String _CSV_COMMODITY_VERSION = "commodity";
    //西藥 禁用物質名
    private final String _CSV_SUBSTANCE_VERSION = "substance";
    //中藥
    private final String _CSV_TRADITION_VERSION = "tradition";
    //資料庫版本(Server)
    private final String _DB_VERSION = "dbVersion";
    //使用者資料庫版本
    private final String _DB_VERSION_USER = "dbVersionUser";


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SharedPreferenceUtil(Context context) {
        sharedPreferences = context.getSharedPreferences(_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    //-----------------------------------------------

    /**
     * 設定 DB版本
     *
     * @param version
     */
    public void setDBVersion(int version) {
        editor.putInt(_DB_VERSION, version).commit();
    }


    //-----------------------------------------------

    /**
     * 取得 DB版本
     *
     * @return
     */
    public int getDBVersion() {
        return sharedPreferences.getInt(_DB_VERSION, 0);
    }

    //-----------------------------------------------

    /**
     * 設定使用者DB版本
     *
     * @param version
     */
    public void setDBVersionUser(int version) {
        editor.putInt(_DB_VERSION_USER, version).commit();
    }

    //-----------------------------------------------

    /**
     * 取得使用者DB版本
     *
     * @return
     */
    public int getDBVersionUser() {
        return sharedPreferences.getInt(_DB_VERSION_USER, 0);
    }

    //-----------------------------------------------


    /**
     * 設定 Commodity版本
     *
     * @param version
     */
    public void setCommodityVersion(int version) {
        editor.putInt(_CSV_COMMODITY_VERSION, version).commit();
    }


    //-----------------------------------------------


    /**
     * 取得 Commodity版本
     *
     * @return
     */
    public int getCommodityVersion() {
        return sharedPreferences.getInt(_CSV_COMMODITY_VERSION, 1);
    }


    //-----------------------------------------------


    /**
     * 設定 Substance版本
     *
     * @param version
     */
    public void setSubstanceVersion(int version) {
        editor.putInt(_CSV_SUBSTANCE_VERSION, version).commit();
    }


    //-----------------------------------------------

    /**
     * 取得 Substance版本
     *
     * @return
     */
    public int getSubstanceVersion() {
        return sharedPreferences.getInt(_CSV_SUBSTANCE_VERSION, 1);
    }


    //-----------------------------------------------

    /**
     * 設定 Tradition版本
     *
     * @param version
     */
    public void setTraditionVersion(int version) {
        editor.putInt(_CSV_TRADITION_VERSION, version).commit();
    }

    //-----------------------------------------------

    /**
     * 取得 Tradition版本
     *
     * @return
     */
    public int getTraditionVersion() {
        return sharedPreferences.getInt(_CSV_TRADITION_VERSION, 1);
    }

    //-----------------------------------------------

    /**
     * 設定 指向的第幾個DB
     *
     * @param dbPath
     */
    public void setDB(String dbPath) {
        editor.putString(_DB, dbPath).commit();
    }

    //-----------------------------------------------

    /**
     * 取得 指向的第幾個DB
     *
     * @return
     */
    public String getDB() {
        return sharedPreferences.getString(_DB, "illeagalDrug3.db");
    }

}
