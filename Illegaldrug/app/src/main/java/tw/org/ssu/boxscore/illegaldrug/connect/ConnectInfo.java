package tw.org.ssu.boxscore.illegaldrug.connect;

import tw.org.ssu.boxscore.illegaldrug.BuildConfig;

/**
 * Created by noel on 2017/6/1.
 */

public class ConnectInfo {

    private static final String WEB = "https://smapp.kmu.edu.tw/";
    private static final String WEB_AZURE = "https://sportillegaldrug.azurewebsites.net/";
    private static final String WEB_DEV = "http://app.creatidea.com.tw/TaipeiSaSportIllegalDrug/";

    //開啟時的權責說明
    //2017
//    public static final String DECLARATION = "https://trello-attachments.s3.amazonaws.com/58d0ed79350d40ffb17bffef/58d2324a5c71850cab3c7527/063e616ec9c3d4e85640bbe0da396981/%E7%A6%81%E8%97%A5%E8%81%B2%E6%98%8E.html";
    //2018
    public static final String DECLARATION = "https://smapp.kmu.edu.tw/IlleagalDrug/";

    public static final String GOOGLE_PLAY = "https://play.google.com/store/apps/details?id=tw.org.ssu.boxscore.illegaldrug";
    //使用者滿意度調查
    public static final String URL_SURVEY = "https://www.surveycake.com/s/wboG6";

    public static final String CSV_DATA = getServerHost() + "api/Drug/Get";

    // ----------------------------------------------------

    /**
     * 取的對應的ConnectServerHost
     *
     * @return
     */
    private static String getServerHost() {

        if (BuildConfig.IS_INTERNAL) {
            return WEB_DEV;
        } else {
            return WEB;
        }
    }

}
