package tw.org.ssu.boxscore.illegaldrug;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;


import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.connect.ConnectInfo;
import tw.org.ssu.boxscore.illegaldrug.util.webview.DeclarationWebView;

/**
 * Created by noel on 2017/3/28.
 */

public class DeclarationDialog extends Dialog {

    private final int TIME_OUT = 3000;

    @BindView(R.id.web_view)
    DeclarationWebView webView;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.btn_ok)
    Button btnOk;

    private OnPrivacyAcceptedListener onPrivacyAcceptedListener;
    private Context context;

    //-----------

    public DeclarationDialog(Context context) {
        super(context);
        this.context = context;

        init();
    }

    //-----------
    private void init() {
        setContentView(R.layout.dialog_declaration);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        btnOk.setBackgroundResource(R.drawable.shape_dialog_btn_nonclick);
        btnOk.setEnabled(false);


        webView.setLoadingMessage(context.getString(R.string.start_page_dialog));
        webView.open(ConnectInfo.DECLARATION);

    }
    //-----------

    @OnCheckedChanged(R.id.checkbox)
    public void onCheckStatusChange(CompoundButton compoundButton, boolean isChecked){
        if (isChecked) {
            checkbox.setTextColor(getContext().getResources().getColor(R.color.splash_clicked_btn));
            btnOk.setBackgroundResource(R.drawable.shape_dialog_btn_clicked);
        } else {
            checkbox.setTextColor(getContext().getResources().getColor(R.color.splash_nonclicked_btn));
            btnOk.setBackgroundResource(R.drawable.shape_dialog_btn_nonclick);
        }
        btnOk.setEnabled(isChecked);
    }


    //-----------
    @OnClick(R.id.btn_ok)
    public void onViewClicked() {
        dismiss();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onPrivacyAcceptedListener.onPrivacyAccepted();
            }
        }, TIME_OUT);
    }

    //-----------
    public interface OnPrivacyAcceptedListener {
        void onPrivacyAccepted();
    }

    //-----------
    public void setOnPrivacyAcceptedListener(OnPrivacyAcceptedListener onPrivacyAcceptedListener) {
        this.onPrivacyAcceptedListener = onPrivacyAcceptedListener;
    }
}
