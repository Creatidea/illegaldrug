package tw.org.ssu.boxscore.illegaldrug.event;


import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tw.org.ssu.boxscore.illegaldrug.startpage.model.CSVData;


/**
 * Created by winni on 2017/3/22.
 */

public class EventCenter {
    //CSV
    public static final int CSV_DATA = 123;

    //--------------------------------------------------
    private static EventCenter ourInstance = new EventCenter();

    //--------------------------------------------------
    public static EventCenter getInstance() {
        return ourInstance;
    }

    //--------------------------------------------------
    private EventCenter() {

    }

    //--------------------------------------------------
    private void sendListEvent(int type, List<?> dataList) {

        Map<String, Object> data = new HashMap<>();
        data.put("type", type);
        data.put("data", dataList);
        EventBus.getDefault().post(data);
    }

    //--------------------------------------------------
    private void sendEvent(int type, Object object) {
        Map<String, Object> data = new HashMap<>();
        data.put("type", type);
        data.put("data", object);
        EventBus.getDefault().post(data);
    }

    //--------------------------------------------------

    /**
     * 傳送 資料庫版本號
     */
    public void sendCSVData(int type, CSVData csvData) {
        sendEvent(type, csvData);
    }

}
