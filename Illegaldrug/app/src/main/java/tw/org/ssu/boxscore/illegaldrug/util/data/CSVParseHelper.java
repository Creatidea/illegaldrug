package tw.org.ssu.boxscore.illegaldrug.util.data;

import android.content.Context;

import com.opencsv.CSVReader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by noel on 2018/5/8.
 */

public class CSVParseHelper {
    private Context context;
    private final int BUFFER_SIZE = 1024;

    public CSVParseHelper(Context context) {
        this.context = context;

    }

    //--------

    /***
     * 解析CSV
     * @param csvFile
     */
    public ArrayList<String[]> parseCSV(String csvFile) {

        ArrayList<String[]> data = new ArrayList<>();
        boolean isNext = true;
        try {

            FileInputStream fileInputStream = context.openFileInput(csvFile);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            //buffer 預留
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream, "utf-8"), BUFFER_SIZE);
            //csvReader套件
            CSVReader csvReader = new CSVReader(bufferedReader);


            //持續讀檔
            while (isNext) {
                //一整行每一個字詞裝入String[]中
                String[] textArray = csvReader.readNext();
                //當空行
                if (textArray == null) {
                    isNext = false;
                }
                //其他
                else {
                    //將資料加入ArrayList<String[]>
                    data.add(textArray);
                }
            }

            //關閉所有串流
            fileInputStream.close();
            bufferedReader.close();
            csvReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }
}
