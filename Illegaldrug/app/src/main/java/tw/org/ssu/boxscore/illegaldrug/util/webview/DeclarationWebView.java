package tw.org.ssu.boxscore.illegaldrug.util.webview;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;

/**
 * Created by noel on 2018/4/19.
 */

public class DeclarationWebView extends CustomWebView {

    public DeclarationWebView(Context context) {
        super(context);
        init();
    }

    public DeclarationWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DeclarationWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    //-------
    private void init() {
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setTextSize(WebSettings.TextSize.LARGEST);

    }
}
