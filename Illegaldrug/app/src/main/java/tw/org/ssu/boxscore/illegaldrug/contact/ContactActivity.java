package tw.org.ssu.boxscore.illegaldrug.contact;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.R;


/**
 * Created by noel on 2018/4/17.
 */

public class ContactActivity extends FragmentActivity {

    @BindView(R.id.view_person)
    View viewPerson;
    @BindView(R.id.view_mail)
    View viewMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);

        TextView tvPersonTitle = ButterKnife.findById(viewPerson, R.id.tv_title);
        TextView tvPersonContent = ButterKnife.findById(viewPerson, R.id.tv_content);
        tvPersonTitle.setText(getString(R.string.contact_subtitle_person));
        tvPersonContent.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_contact_icon, 0, 0, 0);
        tvPersonContent.setCompoundDrawablePadding(15);
        tvPersonContent.setText(getString(R.string.contact_content_person));


        TextView tvMailTitle = ButterKnife.findById(viewMail, R.id.tv_title);
        TextView tvMailContent = ButterKnife.findById(viewMail, R.id.tv_content);
        tvMailTitle.setText(getString(R.string.contact_subtitle_mail));
        tvMailContent.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_contact_icon, 0, 0, 0);
        tvMailContent.setCompoundDrawablePadding(15);
        tvMailContent.setText(getString(R.string.contact_content_mail));

    }

    //--------------------------------------------------
    @OnClick(R.id.back)
    public void onViewClicked() {
        finish();
    }
}
