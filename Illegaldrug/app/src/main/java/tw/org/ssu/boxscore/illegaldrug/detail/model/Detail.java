package tw.org.ssu.boxscore.illegaldrug.detail.model;

/**
 * Created by noel on 2017/4/3.
 */

public class Detail {
    private String title;
    private String content;

    public Detail(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
