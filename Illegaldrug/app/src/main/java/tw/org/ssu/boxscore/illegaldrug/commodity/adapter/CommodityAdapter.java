package tw.org.ssu.boxscore.illegaldrug.commodity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.commodity.CommodityFragment;
import tw.org.ssu.boxscore.illegaldrug.commodity.model.Commodity;

/**
 * Created by noel on 2017/3/29.
 */

public class CommodityAdapter extends BaseAdapter {


    private LayoutInflater inflater;
    private ArrayList<Commodity> commodities;
    private int type = CommodityFragment.TYPE_ZH;

    public CommodityAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        commodities = new ArrayList<>();

    }

    //-----------------------------------------------
    public void setData(ArrayList<Commodity> commodities, int type) {
        this.type = type;
        this.commodities = commodities;
        notifyDataSetChanged();
    }

    //----------------------
    @Override
    public int getCount() {
        return commodities.size();
    }

    //----------------------
    @Override
    public Object getItem(int arg0) {
        return commodities.get(arg0);
    }

    //----------------------
    @Override
    public long getItemId(int position) {
        return position;
    }

    //----------------------
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            //LayoutInflater應用
            convertView = inflater.inflate(R.layout.list_searchcontent, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Commodity commodity = (Commodity) getItem(position);

        switch (type) {
            case CommodityFragment.TYPE_ZH:
                holder.tvDrugName.setText(commodity.getChineseName().equals("") ? commodity.getEnglishName() : commodity.getChineseName());
                break;
            case CommodityFragment.TYPE_EN:
                if (!commodity.getEnglishName().equals("")) {
                    holder.tvDrugName.setText(commodity.getEnglishName());
                }
                break;
        }

        return convertView;
    }

    //----------------------
    class ViewHolder {
        @BindView(R.id.tv_drug_name)
        TextView tvDrugName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
