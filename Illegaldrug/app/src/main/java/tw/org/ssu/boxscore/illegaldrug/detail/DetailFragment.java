package tw.org.ssu.boxscore.illegaldrug.detail;

import android.support.annotation.IntDef;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

import butterknife.BindView;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.commodity.model.Commodity;
import tw.org.ssu.boxscore.illegaldrug.detail.adapter.DetailAdapter;
import tw.org.ssu.boxscore.illegaldrug.detail.model.Detail;
import tw.org.ssu.boxscore.illegaldrug.substance.model.Substance;
import tw.org.ssu.boxscore.illegaldrug.tradition.model.Tradition;
import tw.org.ssu.boxscore.illegaldrug.util.BaseFragment;

/**
 * Created by noel on 2018/4/24.
 */

public class DetailFragment extends BaseFragment {

    //細項說明類別 - 西藥 藥品商品名
    public static final int DETAIL_COMMODITY = 111;
    //細項說明類別 - 西藥 禁用物質名
    public static final int DETAIL_SUBSTANCE = 222;
    //細項說明類別 - 中藥 藥品商品名
    public static final int DETAIL_TRADITION = 333;

    @IntDef({DETAIL_COMMODITY, DETAIL_SUBSTANCE, DETAIL_TRADITION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DetailType {

    }

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.list_view)
    ListView listView;

    private DetailAdapter detailAdapter;
    private int detailType;

    @Override
    protected void init() {
        setView(R.layout.fragment_detail);

        detailType = getArguments().getInt("detailType");
        detailAdapter = new DetailAdapter(mainActivity);
        listView.setAdapter(detailAdapter);
        setData();
    }

    //----------------------

    /***
     * 依照type擺入前一頁面傳來的詳細資料
     */
    private void setData() {

        String title = "";
        ArrayList<Detail> details = new ArrayList<>();
        String[] listTitle = new String[0];
        String[] listContent = new String[0];

        Commodity commodity;
        Substance substance;
        Tradition tradition;

        switch (detailType) {
            //西藥 藥品商品名
            case DETAIL_COMMODITY:
                commodity = getArguments().getParcelable("data");
                listTitle = getResources().getStringArray(R.array.commodity_detail_array);
                listContent = commodity.toArray();
                title = commodity.getChineseName().equals("") ? commodity.getEnglishName() : commodity.getChineseName();
                break;
            //西藥 禁用物質名
            case DETAIL_SUBSTANCE:
                substance = getArguments().getParcelable("data");
                listTitle = getResources().getStringArray(R.array.substance_detail_array);
                listContent = substance.toArray();
                title = substance.getSubstanceName();
                break;
            //中藥 藥品商品名
            case DETAIL_TRADITION:
                tradition = getArguments().getParcelable("data");
                listTitle = getResources().getStringArray(R.array.tradition_detail_array);
                listContent = tradition.toArray();
                title = tradition.getName();
                break;
        }


        for (int index = 0; index < listContent.length; index++) {
            if (listContent[index] != null && !listContent[index].equals("")) {
                details.add(new Detail(listTitle[index], listContent[index]));
            }
        }

        tvName.setText(title);
        detailAdapter.setData(details);
    }
}
