package tw.org.ssu.boxscore.illegaldrug.connect.csv;

import android.content.Context;
import android.widget.Toast;

import com.neilchen.complextoolkit.http.HttpMode;
import com.neilchen.complextoolkit.http.JsonHandler;

import org.json.JSONObject;


import cz.msebera.android.httpclient.Header;
import tw.org.ssu.boxscore.illegaldrug.BuildConfig;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.connect.BaseConnect;
import tw.org.ssu.boxscore.illegaldrug.connect.ConnectInfo;
import tw.org.ssu.boxscore.illegaldrug.event.EventCenter;
import tw.org.ssu.boxscore.illegaldrug.startpage.model.CSVData;

/**
 * Created by noel on 2018/5/4.
 */

public class CSVDataConnect extends BaseConnect {

    private Context context;

    public CSVDataConnect(Context context) {
        super(context);
        this.context = context;
    }

    //--------------------------------------------------

    /***
     *  發起連線 取得各csv最新版本號 與其下載連結
     */
    public void connectToGetCSVData() {

        if (!BuildConfig.IS_INTERNAL) {
            httpControl.setHttpMode(HttpMode.HTTPS_GET);
        }

        httpControl.connect(ConnectInfo.CSV_DATA, null, new JsonHandler() {

            @Override
            public void onSuccessForObject(int statusCode, Header[] headers, JSONObject response) {
                EventCenter.getInstance().sendCSVData(EventCenter.CSV_DATA, parser.getJSONData(response.toString(), CSVData.class));
            }

            @Override
            public void onFailForObject(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
