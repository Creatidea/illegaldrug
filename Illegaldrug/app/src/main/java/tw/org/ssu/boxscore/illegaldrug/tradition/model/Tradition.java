package tw.org.ssu.boxscore.illegaldrug.tradition.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by noel on 2018/4/23.
 */

public class Tradition implements Parcelable {

    //流水號
    private int sid;
    //許可證字號
    private String licenseNumber;
    //禁用物質名
    private String substance;
    //禁用物質分類
    private String category;
    //使用規範
    private String specification;
    //藥品名稱
    private String name;
    //劑型與類別
    private String dosageForm;
    //適應症及效能
    private String indications;
    //成份內容
    private String mainIngredient;
    //備註
    private String note;
    //--------


    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubstance() {
        return substance;
    }

    public void setSubstance(String substance) {
        this.substance = substance;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosageForm() {
        return dosageForm;
    }

    public void setDosageForm(String dosageForm) {
        this.dosageForm = dosageForm;
    }

    public String getIndications() {
        return indications;
    }

    public void setIndications(String indications) {
        this.indications = indications;
    }

    public String getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }


    //資料存取
    public void fromCursor(Cursor c) {
        sid = c.getInt(c.getColumnIndex("sid"));
        substance = c.getString(c.getColumnIndex("Substance"));
        category = c.getString(c.getColumnIndex("Category"));
        specification = c.getString(c.getColumnIndex("Specification"));
        licenseNumber = c.getString(c.getColumnIndex("LicenseNumber"));
        name = c.getString(c.getColumnIndex("Name"));
        dosageForm = c.getString(c.getColumnIndex("DosageForm"));
        indications = c.getString(c.getColumnIndex("Indications"));
        mainIngredient = c.getString(c.getColumnIndex("MainIngredient"));
        note = c.getString(c.getColumnIndex("Note"));
    }

    //--------

    public ContentValues toContentValues() {
        ContentValues ret = new ContentValues();
        ret.put("Substance", getSubstance());
        ret.put("Category", getCategory());
        ret.put("Specification", getSpecification());
        ret.put("LicenseNumber", getLicenseNumber());
        ret.put("Name", getName());
        ret.put("DosageForm", getDosageForm());
        ret.put("Indications", getIndications());
        ret.put("MainIngredient", getMainIngredient());
        ret.put("Note", getNote());

        return ret;
    }
    //--------

    @Override
    public int describeContents() {
        return 0;
    }
    //--------

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sid);
        dest.writeString(this.licenseNumber);
        dest.writeString(this.name);
        dest.writeString(this.dosageForm);
        dest.writeString(this.indications);
        dest.writeString(this.mainIngredient);
        dest.writeString(this.category);
        dest.writeString(this.substance);
        dest.writeString(this.specification);
        dest.writeString(this.note);

    }
    //--------

    public Tradition() {
    }
    //--------

    protected Tradition(Parcel in) {
        this.sid = in.readInt();
        this.licenseNumber = in.readString();
        this.name = in.readString();
        this.dosageForm = in.readString();
        this.indications = in.readString();
        this.mainIngredient = in.readString();
        this.category = in.readString();
        this.substance = in.readString();
        this.specification = in.readString();
        this.note = in.readString();

    }
    //--------

    public static final Parcelable.Creator<Tradition> CREATOR = new Parcelable.Creator<Tradition>() {
        @Override
        public Tradition createFromParcel(Parcel source) {
            return new Tradition(source);
        }
        //--------

        @Override
        public Tradition[] newArray(int size) {
            return new Tradition[size];
        }
    };
    //--------
    /***
     * @return detail 的顯示項目
     */
    public String[] toArray() {
        return new String[]{
                getSubstance(),
                getCategory(),
                getSpecification(),
                getLicenseNumber(),
                getName(),
                getDosageForm(),
                getIndications(),
                getMainIngredient(),
                getNote()
        };
    }
}
