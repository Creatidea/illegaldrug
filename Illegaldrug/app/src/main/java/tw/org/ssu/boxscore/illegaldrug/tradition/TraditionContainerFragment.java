package tw.org.ssu.boxscore.illegaldrug.tradition;

import tw.org.ssu.boxscore.illegaldrug.util.ContainerFragment;

/**
 * Created by noel on 2018/4/17.
 */

public class TraditionContainerFragment extends ContainerFragment{
    @Override
    public void init() {
        replaceFragment(new TraditionFragment(), false);
    }
}
