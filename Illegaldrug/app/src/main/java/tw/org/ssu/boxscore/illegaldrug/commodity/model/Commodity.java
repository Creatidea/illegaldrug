package tw.org.ssu.boxscore.illegaldrug.commodity.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by noel on 2017/3/28.
 */

public class Commodity implements Parcelable {
    private int sid;
    private String substance;
    private String category;
    private String rules;
    private String licenseNumber;
    private String chineseName;
    private String englishName;
    private String dosageForm;
    private String mainIngredient;
    private String applicant;
    private String manufacturer;
    private String engToLowerCase;
    private String note;

    //-----------

    public String getNote() {
        return note;
    }
    //-----------

    public void setNote(String note) {
        this.note = note;
    }
    //-----------

    public String getEngToLowerCase() {
        return engToLowerCase;
    }
    //-----------

    public void setEngToLowerCase(String engToLowerCase) {
        this.engToLowerCase = engToLowerCase;
    }
    //-----------

    public int getSid() {
        return sid;
    }
    //-----------

    public String getSubstance() {
        return substance;
    }
    //-----------

    public String getCategory() {
        return category;
    }
    //-----------

    public String getRules() {
        return rules;
    }
    //-----------

    public String getLicenseNumber() {
        return licenseNumber;
    }
    //-----------

    public String getChineseName() {
        return chineseName;
    }
    //-----------

    public String getEnglishName() {
        return englishName;
    }
    //-----------

    public String getDosageForm() {
        return dosageForm;
    }
    //-----------

    public String getMainIngredient() {
        return mainIngredient;
    }
    //-----------

    public String getApplicant() {
        return applicant;
    }
    //-----------

    public String getManufacturer() {
        return manufacturer;
    }
    //-----------

    public void setSid(int sid) {
        this.sid = sid;
    }
    //-----------

    public void setSubstance(String substance) {
        this.substance = substance;
    }
    //-----------

    public void setCategory(String category) {
        this.category = category;
    }
    //-----------

    public void setRules(String rules) {
        this.rules = rules;
    }
    //-----------

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }
    //-----------

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }
    //-----------

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
    //-----------

    public void setDosageForm(String dosageForm) {
        this.dosageForm = dosageForm;
    }
    //-----------

    public void setMainIngredient(String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }
    //-----------

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }
    //-----------

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    //-----------

    /***
     * 資料取出
     * @param c
     */
    public void fromCursor(Cursor c) {
        sid = c.getInt(c.getColumnIndex("sid"));
        substance = c.getString(c.getColumnIndex("Substance"));
        category = c.getString(c.getColumnIndex("Category"));
        rules = c.getString(c.getColumnIndex("Rules"));
        licenseNumber = c.getString(c.getColumnIndex("LicenseNumber"));
        chineseName = c.getString(c.getColumnIndex("ChineseName"));
        englishName = c.getString(c.getColumnIndex("EnglishName"));
        dosageForm = c.getString(c.getColumnIndex("DosageForm"));
        mainIngredient = c.getString(c.getColumnIndex("MainIngredient"));
        applicant = c.getString(c.getColumnIndex("Applicant"));
        manufacturer = c.getString(c.getColumnIndex("Manufacturer"));
        note = c.getString(c.getColumnIndex("Note"));
        engToLowerCase = englishName.toLowerCase();
    }

    //-----------

    /***
     * 資料存入
     * @return
     */
    public ContentValues toContentValues() {
        ContentValues ret = new ContentValues();
        ret.put("Substance", getSubstance());
        ret.put("Category", getCategory());
        ret.put("Rules", getRules());
        ret.put("LicenseNumber", getLicenseNumber());
        ret.put("ChineseName", getChineseName());
        ret.put("EnglishName", getEnglishName());
        ret.put("DosageForm", getDosageForm());
        ret.put("MainIngredient", getMainIngredient());
        ret.put("Applicant", getApplicant());
        ret.put("Manufacturer", getManufacturer());
        ret.put("Note",getNote());
        return ret;
    }
    //-----------

    @Override
    public int describeContents() {
        return 0;
    }
    //-----------

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sid);
        dest.writeString(this.substance);
        dest.writeString(this.category);
        dest.writeString(this.rules);
        dest.writeString(this.licenseNumber);
        dest.writeString(this.chineseName);
        dest.writeString(this.englishName);
        dest.writeString(this.dosageForm);
        dest.writeString(this.mainIngredient);
        dest.writeString(this.applicant);
        dest.writeString(this.manufacturer);
        dest.writeString(this.engToLowerCase);
        dest.writeString(this.note);

    }
    //-----------

    public Commodity() {
    }
    //-----------

    protected Commodity(Parcel in) {
        this.sid = in.readInt();
        this.substance = in.readString();
        this.category = in.readString();
        this.rules = in.readString();
        this.licenseNumber = in.readString();
        this.chineseName = in.readString();
        this.englishName = in.readString();
        this.dosageForm = in.readString();
        this.mainIngredient = in.readString();
        this.applicant = in.readString();
        this.manufacturer = in.readString();
        this.engToLowerCase = in.readString();
        this.note = in.readString();
    }
    //-----------

    public static final Parcelable.Creator<Commodity> CREATOR = new Parcelable.Creator<Commodity>() {
        @Override
        public Commodity createFromParcel(Parcel source) {
            return new Commodity(source);
        }
        //--------

        @Override
        public Commodity[] newArray(int size) {
            return new Commodity[size];
        }
    };
    //-----------

    /***
     * @return detail 的顯示項目
     */
    public String[] toArray() {
        return new String[]{
                getSubstance(),
                getCategory(),
                getRules(),
                getLicenseNumber(),
                getChineseName(),
                getEnglishName(),
                getDosageForm(),
                getMainIngredient(),
                getApplicant(),
                getManufacturer(),
                getNote()
        };
    }

}
