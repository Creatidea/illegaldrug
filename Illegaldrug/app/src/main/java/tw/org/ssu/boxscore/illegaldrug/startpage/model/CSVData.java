package tw.org.ssu.boxscore.illegaldrug.startpage.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by noel on 2018/5/4.
 */

public class CSVData {


    /**
     * Version : 1
     * Drugs : [{"Path":"http://app.creatidea.com.tw/TaipeiSaSportIllegalDrug/Uploads/FileDrugtradition/E98FF15F-9AC9-41EF-A0E2-217ADFB19F11.csv","DrugType":"tradition","Version":1,"CreateTime":"2018/05/08 18:02"},{"Path":"http://app.creatidea.com.tw/TaipeiSaSportIllegalDrug/Uploads/FileDrugcommodity/74B125F1-C4EE-4894-8755-99A888DB6124.csv","DrugType":"commodity","Version":1,"CreateTime":"2018/05/08 18:02"},{"Path":"http://app.creatidea.com.tw/TaipeiSaSportIllegalDrug/Uploads/FileDrugsubstance/4FCC8857-0F01-4B8A-948B-3735783B9DCA.csv","DrugType":"substance","Version":1,"CreateTime":"2018/05/08 18:03"}]
     */

    @SerializedName("DbVersion")
    private int dbVersion;
    @SerializedName("Drugs")
    private ArrayList<DrugsBean> drugs;

    public int getDbVersion() {
        return dbVersion;
    }

    public void setDbVersion(int dbVersion) {
        this.dbVersion = dbVersion;
    }

    public ArrayList<DrugsBean> getDrugs() {
        return drugs;
    }

    public void setDrugs(ArrayList<DrugsBean> drugs) {
        this.drugs = drugs;
    }

    public static class DrugsBean {
        /**
         * Path : http://app.creatidea.com.tw/TaipeiSaSportIllegalDrug/Uploads/FileDrugtradition/E98FF15F-9AC9-41EF-A0E2-217ADFB19F11.csv
         * DrugType : tradition
         * Version : 1
         * CreateTime : 2018/05/08 18:02
         */

        @SerializedName("Path")
        private String path;
        @SerializedName("DrugType")
        private String drugType;
        @SerializedName("Version")
        private int version;
        @SerializedName("CreateTime")
        private String createTime;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getDrugType() {
            return drugType;
        }

        public void setDrugType(String drugType) {
            this.drugType = drugType;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
