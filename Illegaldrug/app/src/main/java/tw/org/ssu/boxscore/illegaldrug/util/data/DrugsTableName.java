package tw.org.ssu.boxscore.illegaldrug.util.data;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class DrugsTableName {

    //西藥 藥品商品名
    public static final String TABLE_NAME_COMMODITY = "AllMedicines";
    //西藥 禁用物質名
    public static final String TABLE_NAME_SUBSTANCE = "SubstanceNameSearch";
    //中藥 藥品商品名
    public static final String TABLE_NAME_TRADITION = "Tradition";


    @StringDef({TABLE_NAME_COMMODITY, TABLE_NAME_SUBSTANCE, TABLE_NAME_TRADITION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TableName {
    }

}
