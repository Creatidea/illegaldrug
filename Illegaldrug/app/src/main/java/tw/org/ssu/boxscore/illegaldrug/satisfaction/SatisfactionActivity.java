package tw.org.ssu.boxscore.illegaldrug.satisfaction;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.connect.ConnectInfo;
import tw.org.ssu.boxscore.illegaldrug.util.webview.CustomWebView;

/**
 * Created by noel on 2018/4/17.
 */

public class SatisfactionActivity extends FragmentActivity {

    @BindView(R.id.web_view)
    CustomWebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satisfaction);
        ButterKnife.bind(this);
        webView.setLoadingMessage(getString(R.string.satisfaction_title));
        webView.open(ConnectInfo.URL_SURVEY);

    }

    //--------

    @OnClick(R.id.back)
    public void onViewClicked() {
        finish();
    }
}
