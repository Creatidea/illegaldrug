package tw.org.ssu.boxscore.illegaldrug.substance;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import tw.org.ssu.boxscore.illegaldrug.detail.DetailFragment;
import tw.org.ssu.boxscore.illegaldrug.substance.adapter.SubstanceAdapter;
import tw.org.ssu.boxscore.illegaldrug.util.data.DAOUtil;
import tw.org.ssu.boxscore.illegaldrug.substance.model.Substance;
import tw.org.ssu.boxscore.illegaldrug.util.BaseFragment;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.util.dialog.LoadingDialog;

/**
 * 西藥禁用物質
 */
public class SubstanceFragment extends BaseFragment implements AdapterView.OnItemClickListener, TextWatcher {

    //db資料
    private DAOUtil dao;
    private ArrayList<Substance> substances = new ArrayList<>();
    //搜尋的資料
    private ArrayList<Substance> searchSubstance;
    private SubstanceAdapter adapter;
    private String substanceName;

    @BindView(R.id.listView)
    StickyListHeadersListView listView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.editText)
    EditText editText;

    @Override
    protected void init() {

        setView(R.layout.fragment_substance);

        dao = new DAOUtil(mainActivity);
        adapter = new SubstanceAdapter(mainActivity);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        editText.setHint(getString(R.string.substance_hint));
        imageView.setImageResource(R.drawable.icon_search);

        editText.addTextChangedListener(this);

        if (substances.size() == 0) {//第一次進來這頁面的時候
            new GetAllDatas().execute();
        }
    }

    // ----------------------------------------------------
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    // ----------------------------------------------------
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        substanceName = editText.getText().toString();//取得輸入字串

        if (substanceName.equals("")) {
            imageView.setImageResource(R.drawable.icon_search);
            if (substances.size() == 0) {
                new GetAllDatas().execute();
            } else {
                adapter.setData(substances);
            }
        } else {
            imageView.setImageResource(R.drawable.icon_x);
            searchSubstance = dao.getSubstanceLikeName(substanceName);
            adapter.setData(searchSubstance);
        }
    }

    // ----------------------------------------------------
    @Override
    public void afterTextChanged(Editable s) {

    }

    // ----------------------------------------------------
    @OnClick({R.id.imageView})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.imageView:
                editText.setText("");
                closeKeyboard();//關閉Keyboard
                break;
        }
    }

    // ----------------------------------------------------
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        closeKeyboard();

        Bundle bundle = new Bundle();
        Substance substance = (Substance) parent.getAdapter().getItem(position);
        bundle.putParcelable("data", substance);
        bundle.putInt("detailType", DetailFragment.DETAIL_SUBSTANCE);
        replaceFragment3(new DetailFragment(), true, bundle);
    }

    //-----------------------------------------------
    private class GetAllDatas extends AsyncTask<Void, Void, Void> {

        private LoadingDialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = new LoadingDialog(mainActivity);
            loadingDialog.setLoadingMessage(getString(R.string.text_loading));
            loadingDialog.showLoadingDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            substances = dao.getSubstanceData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }
            adapter.setData(substances);
        }
    }
}