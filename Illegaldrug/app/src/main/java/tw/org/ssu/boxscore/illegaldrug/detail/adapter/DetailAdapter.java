package tw.org.ssu.boxscore.illegaldrug.detail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.detail.model.Detail;

/**
 * Created by noel on 2017/3/31.
 */

public class DetailAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<Detail> details;


    public DetailAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        details = new ArrayList<>();
    }

    //------------

    @Override
    public int getCount() {
        return details.size();
    }
    //------------

    @Override
    public Object getItem(int position) {
        return details.get(position);
    }
    //------------

    @Override
    public long getItemId(int position) {
        return position;
    }
    //------------

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            //LayoutInflater應用
            convertView = inflater.inflate(R.layout.list_detail, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        Detail detail = (Detail) getItem(position);
        holder.title.setText(detail.getTitle());
        holder.content.setText(detail.getContent());
        return convertView;
    }

    //------------
    public void setData(ArrayList<Detail> details) {
        this.details = details;
        notifyDataSetChanged();
    }

    //------------

    static class ViewHolder {

        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.title)
        TextView title;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
