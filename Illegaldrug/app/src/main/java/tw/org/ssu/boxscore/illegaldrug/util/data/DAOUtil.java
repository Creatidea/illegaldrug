package tw.org.ssu.boxscore.illegaldrug.util.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import tw.org.ssu.boxscore.illegaldrug.commodity.model.Commodity;
import tw.org.ssu.boxscore.illegaldrug.substance.model.Substance;
import tw.org.ssu.boxscore.illegaldrug.tradition.model.Tradition;
import tw.org.ssu.boxscore.illegaldrug.util.sharedpreference.SharedPreferenceUtil;

import static tw.org.ssu.boxscore.illegaldrug.util.data.DrugsTableName.TABLE_NAME_COMMODITY;
import static tw.org.ssu.boxscore.illegaldrug.util.data.DrugsTableName.TABLE_NAME_SUBSTANCE;
import static tw.org.ssu.boxscore.illegaldrug.util.data.DrugsTableName.TABLE_NAME_TRADITION;


/**
 * Created by noel on 2017/3/28.
 */

public class DAOUtil {

    private final String DEFAULT_DB_NAME = "illeagalDrug3.db";
    private final int DB_VERSION = 1;
    private final int BUFFER_SIZE = 1024;

    private OnInsertedDataListener onInsertedDataListener;
    private Context context;
    private SharedPreferenceUtil sp;

    public DAOUtil(Context context) {
        this.context = context;
        sp = new SharedPreferenceUtil(context);
    }

    // ----------------------------------------------------

    /**
     * 取得所有西藥 藥品商品名並依照sid排序
     *
     * @return
     */
    public ArrayList<Commodity> getCommodityData() {

        SQLiteDatabase db = openDatabase();

        String sql = "SELECT * FROM AllMedicines ORDER BY sid";
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Commodity> commodities = new ArrayList<Commodity>();
        if (c.moveToFirst()) {
            do {
                Commodity commodity = new Commodity();
                commodity.fromCursor(c);
                commodities.add(commodity);
            } while (c.moveToNext());
        }
        c.close();
        db.close();

        return commodities;
    }

    // ----------------------------------------------------

    /***
     * 取得所有西藥 禁用物質並依照sid排序
     * @return
     */

    public ArrayList<Substance> getSubstanceData() {

        SQLiteDatabase db = openDatabase();
        String sql = "SELECT * FROM SubstanceNameSearch ORDER BY sid";
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Substance> substances = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Substance substance = new Substance();
                substance.fromCursor(c);
                substances.add(substance);
            } while (c.moveToNext());
        }
        c.close();
        db.close();

        return substances;
    }

    // ----------------------------------------------------

    /**
     * 取得所有中藥 藥品商品名並依照sid排序
     *
     * @return
     */
    public ArrayList<Tradition> getTraditionData() {

        SQLiteDatabase db = openDatabase();
        ArrayList<Tradition> traditions = new ArrayList<Tradition>();

        String sql = "SELECT * FROM Tradition ORDER BY sid";
        Cursor c = db.rawQuery(sql, null);
        if (c.moveToFirst()) {
            do {
                Tradition tradition = new Tradition();
                tradition.fromCursor(c);
                traditions.add(tradition);
            } while (c.moveToNext());
        }
        c.close();
        db.close();

        return traditions;
    }


    // ----------------------------------------------------

    /**
     * 西藥藥品商品名 模糊搜尋 ChineseName
     *
     * @param chineseName
     * @return
     */
    public ArrayList<Commodity> getCommodityByChineseName(String chineseName) {

        SQLiteDatabase db = openDatabase();
        //對多個欄位進行模糊搜尋
        chineseName = chineseName.replace("'", "''");//避免單引號搜尋

        String sql = "SELECT * FROM AllMedicines WHERE  ChineseName LIKE '%" + chineseName + "%' ORDER BY sid";
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Commodity> commodities = new ArrayList<Commodity>();
        if (c.moveToFirst()) {
            do {
                Commodity commodity = new Commodity();
                commodity.fromCursor(c);
                commodities.add(commodity);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return commodities;
    }

    // ----------------------------------------------------

    /**
     * 西藥藥品商品名 模糊搜尋 EnglishName
     *
     * @param englishName
     * @return
     */
    public ArrayList<Commodity> getCommodityByEnglishName(String englishName) {

        SQLiteDatabase db = openDatabase();
        //對多個欄位進行模糊搜尋
        englishName = englishName.replace("'", "''");//避免單引號搜尋

        String sql = "SELECT * FROM AllMedicines WHERE  EnglishName LIKE '%" + englishName + "%' ORDER BY sid";
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Commodity> commodities = new ArrayList<Commodity>();
        if (c.moveToFirst()) {
            do {
                Commodity commodity = new Commodity();
                commodity.fromCursor(c);
                commodities.add(commodity);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return commodities;
    }


    // ----------------------------------------------------

    /**
     * 西藥藥品商品名 取得符合帶入Sid值的藥物
     *
     * @param sid
     * @return
     */
    public Commodity getCommodityBySid(int sid) {

        SQLiteDatabase db = openDatabase();
        String sql = "SELECT * FROM AllMedicines WHERE sid = " + sid;
        Cursor c = db.rawQuery(sql, null);
        Commodity commodity = new Commodity();
        if (c.moveToFirst()) {
            commodity.fromCursor(c);
        }
        c.close();
        db.close();
        return commodity;
    }


    // ----------------------------------------------------

    /***
     *  西藥 禁用物質名 中搜尋指定sid
     * @param sid
     * @return
     */
    public Substance getSubstanceBySid(int sid) {

        SQLiteDatabase db = openDatabase();
        String sql = "SELECT * FROM SubstanceNameSearch WHERE sid = " + sid;
        Cursor c = db.rawQuery(sql, null);
        Substance substance = new Substance();
        if (c.moveToFirst()) {
            substance.fromCursor(c);
        }
        c.close();
        db.close();
        return substance;
    }


    // ----------------------------------------------------

    /**
     * 西藥 禁用物質名 模糊搜尋 SubstanceName
     *
     * @param substanceName
     * @return
     */
    public ArrayList<Substance> getSubstanceLikeName(String substanceName) {

        SQLiteDatabase db = openDatabase();

        substanceName = substanceName.replace("'", "''");//避免單引號搜尋
        String sql = "SELECT  * FROM SubstanceNameSearch WHERE  SubstanceName  LIKE '%" + substanceName + "%'";
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Substance> substances = new ArrayList<Substance>();
        if (c.moveToFirst()) {
            do {
                Substance substance = new Substance();

                substance.fromCursor(c);
                substances.add(substance);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return substances;
    }


    // ----------------------------------------------------

    /***
     *  insert 進入 Commodity
     *  AllMedicines: table name
     */
    public void insertCommodity(ArrayList<String[]> listCommodity) {

        SQLiteDatabase db = openDatabase();

        db.beginTransaction();

        try {

            Commodity commodity;
            //第0比不用因為是標題
            for (int i = 1; i < listCommodity.size(); i++) {
                if (listCommodity.get(i)[0] != null && !listCommodity.get(i)[0].equals("")) {
                    commodity = new Commodity();
                    // 禁用物質名,(行)[列]
                    commodity.setSubstance(listCommodity.get(i)[0]);
                    //禁用物質分類,(行)[列]
                    commodity.setCategory(listCommodity.get(i)[1]);
                    //使用規範,(行)[列]
                    commodity.setRules(listCommodity.get(i)[2]);
                    //許可證字號,(行)[列]
                    commodity.setLicenseNumber(listCommodity.get(i)[3]);
                    //中文品名,(行)[列]
                    commodity.setChineseName(listCommodity.get(i)[4]);
                    //英文品名,(行)[列]
                    commodity.setEnglishName(listCommodity.get(i)[5]);
                    //劑型,(行)[列]
                    commodity.setDosageForm(listCommodity.get(i)[6]);
                    //主成分略述,(行)[列]
                    commodity.setMainIngredient(listCommodity.get(i)[7]);
                    //申請商名稱,(行)[列]
                    commodity.setApplicant(listCommodity.get(i)[8]);
                    //製造商名稱,(行)[列]
                    commodity.setManufacturer(listCommodity.get(i)[9]);
                    //備註,(行)[列]
                    commodity.setNote(listCommodity.get(i)[10]);

                    db.insert(TABLE_NAME_COMMODITY, null, commodity.toContentValues());

                    if (onInsertedDataListener != null) {
                        onInsertedDataListener.OnInsertedData();
                    }
                }
            }

            db.setTransactionSuccessful();

        } catch (Exception ex) {
            Log.e("SQLITE", ex.getMessage());
        }

        db.endTransaction();
        db.close();

    }


    // ----------------------------------------------------

    /***
     *  insert 進入 Substance
     *  table Name : SubstanceNameSearch
     */
    public void insertSubstance(ArrayList<String[]> listSubstance) {

        SQLiteDatabase db = openDatabase();

        db.beginTransaction();

        try {

            Substance substance;
            //第0比不用因為是標題
            for (int i = 1; i < listSubstance.size(); i++) {
                if (listSubstance.get(i)[0] != null && !listSubstance.get(i)[0].equals("")) {

                    substance = new Substance();
                    //禁用物質名
                    substance.setSubstanceName(listSubstance.get(i)[0]);
                    //禁用物質分類
                    substance.setCategory(listSubstance.get(i)[1]);
                    //禁用物質
                    substance.setSubstance(listSubstance.get(i)[2]);
                    //使用規範
                    substance.setRules(listSubstance.get(i)[3]);
                    //說明
                    substance.setManual(listSubstance.get(i)[4]);
                    //備註
                    substance.setNote(listSubstance.get(i)[5]);

                    db.insert(TABLE_NAME_SUBSTANCE, null, substance.toContentValues());

                    if (onInsertedDataListener != null) {
                        onInsertedDataListener.OnInsertedData();
                    }
                }
            }

            db.setTransactionSuccessful();

        } catch (Exception ex) {
            Log.e("SQLITE", ex.getMessage());
        }

        db.endTransaction();
        db.close();

    }


    // ----------------------------------------------------

    /***
     *  insert 進入 Tradition
     */
    public void insertTradition(ArrayList<String[]> listTradition) {

        SQLiteDatabase db = openDatabase();

        db.beginTransaction();

        try {

            Tradition tradition;

            //第0比不用因為是標題
            for (int i = 1; i < listTradition.size(); i++) {
                if (listTradition.get(i)[0] != null && !listTradition.get(i)[0].equals("")) {
                    tradition = new Tradition();
                    //禁用物質名
                    tradition.setSubstance(listTradition.get(i)[0]);
                    //禁用物質分類
                    tradition.setCategory(listTradition.get(i)[1]);
                    //使用規範
                    tradition.setSpecification(listTradition.get(i)[2]);
                    //許可證字號
                    tradition.setLicenseNumber(listTradition.get(i)[3]);
                    //藥品名稱
                    tradition.setName(listTradition.get(i)[4]);
                    //劑型與類別
                    tradition.setDosageForm(listTradition.get(i)[5]);
                    //適應症及效能
                    tradition.setIndications(listTradition.get(i)[6]);
                    //成份內容
                    tradition.setMainIngredient(listTradition.get(i)[7]);
                    //備註
                    tradition.setNote(listTradition.get(i)[8]);

                    db.insert(TABLE_NAME_TRADITION, null, tradition.toContentValues());

                    if (onInsertedDataListener != null) {
                        onInsertedDataListener.OnInsertedData();
                    }
                }
            }

            db.setTransactionSuccessful();

        } catch (Exception ex) {
            Log.e("SQLITE", ex.getMessage());
        }

        db.endTransaction();
        db.close();

    }


    // ----------------------------------------------------

    /**
     * 更新 西藥藥品商品名
     *
     * @param commodity
     */
    public void updateCommodity(Commodity commodity) {

        SQLiteDatabase db = openDatabase();
        try {
            db.update("AllMedicines", commodity.toContentValues(), "sid = ?", new String[]{commodity.getSid() + ""});
        } catch (Exception ex) {
            Log.e("SQLITE", ex.getMessage());
        }
        db.close();
    }

    // ----------------------------------------------------

    /***
     * 更新 西藥禁用物質名
     * @param substance
     */
    public void updateSubstance(Substance substance) {

        SQLiteDatabase db = openDatabase();
        try {
            db.update("SubstanceNameSearch", substance.toContentValues(), "sid = ?", new String[]{substance.getSid() + ""});
        } catch (Exception ex) {
            Log.e("SQLITE", ex.getMessage());
        }
        db.close();
    }


    // ----------------------------------------------------

    /**
     * 複製DB
     *
     * @param dbFile
     * @throws IOException
     */
    private void copyDatabase(File dbFile) {

        File parentDir = new File(dbFile.getParent());
        if (!parentDir.exists()) {
            parentDir.mkdir();
        }

        try {

            InputStream is = context.getAssets().open(DEFAULT_DB_NAME);
            OutputStream os = new FileOutputStream(dbFile);

            byte[] buffer = new byte[BUFFER_SIZE];
            int read = is.read(buffer);
            while (read != -1) {
                os.write(buffer, 0, read);
                read = is.read(buffer);
            }

            os.flush();
            os.close();
            is.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------

    /**
     * 開啟DB
     *
     * @return
     */
    private SQLiteDatabase openDatabase() {

        if (sp.getDBVersionUser() != DB_VERSION) {
            File dbFile = context.getDatabasePath(DEFAULT_DB_NAME);
            goToCopyDB(dbFile);
        }

        File dbFile = context.getDatabasePath(DEFAULT_DB_NAME);
        SQLiteDatabase database = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

        if (database == null) {
            goToCopyDB(dbFile);
            database = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
        }

        return database;
    }

    // ----------------------------------------------------

    /**
     * 前往複製DB即將先把原本DB砍掉(以避免取得舊有DB會死亡問題)
     *
     * @param dbFile
     */
    private void goToCopyDB(File dbFile) {
        context.deleteDatabase(sp.getDB());
        copyDatabase(dbFile);
        sp.setDBVersionUser(DB_VERSION);
    }

    // ----------------------------------------------------

    /***
     * 全部Table的值清空
     */
    public void clear() {
        clearTable(DrugsTableName.TABLE_NAME_TRADITION);
        clearTable(DrugsTableName.TABLE_NAME_SUBSTANCE);
        clearTable(DrugsTableName.TABLE_NAME_COMMODITY);
    }

    // ----------------------------------------------------

    /**
     * 清除選擇的Table
     *
     * @param tableName
     */
    public void clearTable(@DrugsTableName.TableName String tableName) {

        SQLiteDatabase db = openDatabase();

        switch (tableName) {

            case TABLE_NAME_COMMODITY:
                db.execSQL("DELETE FROM `AllMedicines`");
                break;
            case TABLE_NAME_SUBSTANCE:
                db.execSQL("DELETE FROM `SubstanceNameSearch`");
                break;
            case TABLE_NAME_TRADITION:
                db.execSQL("DELETE FROM `Tradition`");
                break;
        }

        db.close();

    }

    // ----------------------------------------------------

    /***
     *  當insert data
     */
    public interface OnInsertedDataListener {
        void OnInsertedData();
    }

    // ----------------------------------------------------
    public void setOnInsertedDataListener(OnInsertedDataListener onInsertedDataListener) {
        this.onInsertedDataListener = onInsertedDataListener;
    }

}
