package tw.org.ssu.boxscore.illegaldrug.util;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import butterknife.ButterKnife;
import tw.org.ssu.boxscore.illegaldrug.MainActivity;

/**
 * Created by noel on 2017/4/21.
 */

public abstract class BaseFragment extends Fragment {

    // ----------------------------------------------------
    protected MainActivity mainActivity;
    protected View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            mainActivity = (MainActivity) context;
        }
    }

    // ----------------------------------------------------
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            // initial.
            init();
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        return view;
    }

    // ----------------------------------------------------

    /**
     * initial
     */
    protected abstract void init();


    // ----------------------------------------------------
    public void setView(int resource) {
        this.view = LayoutInflater.from(getActivity()).inflate(resource, null);
        ButterKnife.bind(this, view);
    }

    //----------------------------------------------

    /***
     *  替換第一層 container
     * @param fragment
     * @param addToBackStack
     */
    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(android.R.id.tabcontent, fragment);
        transaction.commit();
        getChildFragmentManager().executePendingTransactions();
    }

    //----------------------------------------------

    /***
     * 替換 第二層以後 有bundle
     * @param fragment
     * @param addToBackStack
     * @param bundle
     */
    public void replaceFragment3(Fragment fragment, boolean addToBackStack, Bundle bundle) {
        fragment.setArguments(bundle);

        FragmentTransaction transaction = getParentFragment().getChildFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(android.R.id.tabcontent, fragment);
        transaction.commit();
        getParentFragment().getChildFragmentManager().executePendingTransactions();
    }

    //----------------------------------------------

    /***
     *  如果有前一頁 則 回到前一頁
     * @return
     */
    public boolean popFragment() {
        boolean isPop = false;
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            isPop = true;
            getChildFragmentManager().popBackStack();
        }
        return isPop;
    }

    //--------------------

    /***
     * 關閉虛擬鍵盤
     */
    protected void closeKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
