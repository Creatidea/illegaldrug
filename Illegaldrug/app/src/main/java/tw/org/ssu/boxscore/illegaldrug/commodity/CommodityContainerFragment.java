package tw.org.ssu.boxscore.illegaldrug.commodity;

import tw.org.ssu.boxscore.illegaldrug.util.ContainerFragment;

/**
 * Created by noel on 2017/4/25.
 */

public class CommodityContainerFragment extends ContainerFragment {
    // ---------------------------------------------------
    @Override
    public void init() {
        replaceFragment(new CommodityFragment(), false);
    }
    // ---------------------------------------------------
}
