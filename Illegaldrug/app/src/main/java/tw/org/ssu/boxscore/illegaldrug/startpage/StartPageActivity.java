package tw.org.ssu.boxscore.illegaldrug.startpage;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.neilchen.complextoolkit.util.checkversion.CheckUpdateActionListener;
import com.neilchen.complextoolkit.util.checkversion.CheckVersionManager;
import com.neilchen.complextoolkit.util.customdialog.CustomDialog;

import io.fabric.sdk.android.Fabric;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import tw.org.ssu.boxscore.illegaldrug.DeclarationDialog;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.connect.ConnectInfo;
import tw.org.ssu.boxscore.illegaldrug.connect.csv.CSVDataConnect;
import tw.org.ssu.boxscore.illegaldrug.entrance.EntranceActivity;
import tw.org.ssu.boxscore.illegaldrug.event.EventCenter;
import tw.org.ssu.boxscore.illegaldrug.startpage.model.CSVData;
import tw.org.ssu.boxscore.illegaldrug.util.data.CSVParseHelper;
import tw.org.ssu.boxscore.illegaldrug.connect.csv.downloader.CSVDownLoader;
import tw.org.ssu.boxscore.illegaldrug.connect.csv.downloader.OnSavedInInternalStorageListener;
import tw.org.ssu.boxscore.illegaldrug.util.sharedpreference.SharedPreferenceUtil;
import tw.org.ssu.boxscore.illegaldrug.util.data.DAOUtil;

import static tw.org.ssu.boxscore.illegaldrug.startpage.CSVDrugType.CSV_COMMODITY;
import static tw.org.ssu.boxscore.illegaldrug.startpage.CSVDrugType.CSV_SUBSTANCE;
import static tw.org.ssu.boxscore.illegaldrug.startpage.CSVDrugType.CSV_TRADITION;
import static tw.org.ssu.boxscore.illegaldrug.util.data.DrugsTableName.TABLE_NAME_COMMODITY;
import static tw.org.ssu.boxscore.illegaldrug.util.data.DrugsTableName.TABLE_NAME_SUBSTANCE;
import static tw.org.ssu.boxscore.illegaldrug.util.data.DrugsTableName.TABLE_NAME_TRADITION;

public class StartPageActivity extends AppCompatActivity implements DeclarationDialog.OnPrivacyAcceptedListener,
        OnSavedInInternalStorageListener, DAOUtil.OnInsertedDataListener {

    private final int HANDLER_MOTION_SUCCESS_CHECK_CSV = 111;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.tv_progress)
    TextView tvProgress;

    private CSVData csvData;
    private CSVDataConnect csvDataConnect;
    private CSVDownLoader csvDownLoader;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private Runnable runnable;
    private Handler handler;
    // App版本控制
    private CheckVersionManager checkVersionManager;

    //db資料
    private DAOUtil dao;
    //csv parser
    private CSVParseHelper csvParseHelper;

    //載入進度
    private int progress = 0;
    private DeclarationDialog declarationDialog;
    private HashMap<String, CSVData.DrugsBean> csvDataDropMap;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this);
        setContentView(R.layout.activity_start_page);
        ButterKnife.bind(this);

        csvParseHelper = new CSVParseHelper(this);
        dao = new DAOUtil(this);
        dao.setOnInsertedDataListener(this);
        sharedPreferenceUtil = new SharedPreferenceUtil(this);
        checkVersionManager = new CheckVersionManager(this);
        declarationDialog = new DeclarationDialog(this);
        declarationDialog.setOnPrivacyAcceptedListener(this);
        csvDataConnect = new CSVDataConnect(this);
        csvDownLoader = new CSVDownLoader(this);
        csvDownLoader.setOnSavedInInternalStorageListener(this);

        checkVersionManager.setForceUpdate(true);
        setUpdateDialog(checkVersionManager.dialog);
        initRunnable();

        checkVersionManager.checkGooglePlay(ConnectInfo.GOOGLE_PLAY, new CheckUpdateActionListener() {
            @Override
            public void onLatestListener() {
//                如果要進入到內頁查看其他功能解開這裡的註解
//                declarationDialog.show();
//                並且註解下面那行即可
                csvDataConnect.connectToGetCSVData();
            }
        });
    }

    //--------------------------------------------------------

    /***
     *  更新時所需
     */
    private void initRunnable() {

        runnable = new Runnable() {
            @Override
            public void run() {
                if (sharedPreferenceUtil.getDBVersion() < csvData.getDbVersion()) {
                    //執行將資料寫入資料庫之動作
                    new getDatas().execute();
                } else {
                    tvProgress.setText("");
                    declarationDialog.show();
                }
            }
        };

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.arg1) {
                    case HANDLER_MOTION_SUCCESS_CHECK_CSV:
                        handler.post(runnable);
                        break;
                }
            }
        };
    }

    //--------------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    //--------------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    //--------------------------------------------------------
    @Subscribe
    public void onSuccessConnect(Map<Integer, Object> data) {

        if ((int) data.get("type") == EventCenter.CSV_DATA) {
            csvData = (CSVData) data.get("data");
            tvProgress.setVisibility(View.VISIBLE);
            csvDataDropMap = getCSVDataDropMap(csvData.getDrugs());
            checkCSVVersion(CSV_TRADITION);
        }
    }

    //--------------------------------------------------------

    /***
     *  當成功下載且儲存至內部資料夾
     * @param responseCode 連線狀態碼
     * @param absolutePath 絕對路徑 內部資料夾/fileName.xxx
     * @param fileName     檔名.xxx
     */
    @Override
    public void onSuccessSaved(int responseCode, String absolutePath, String fileName) {

        switch (fileName) {
            //當成功下載中藥藥品商品名
            case CSVDownLoader.CSV_FILE_TRADITION:
                checkCSVVersion(CSV_SUBSTANCE);
                break;
            //當成功下載西藥禁用物質名
            case CSVDownLoader.CSV_FILE_SUBSTANCE:
                checkCSVVersion(CSV_COMMODITY);
                break;
            //當成功下載西藥藥品商品名
            case CSVDownLoader.CSV_FILE_COMMODITY:
                goToInsertData();
                break;
        }
    }

    //--------------------------------------------------------
    @Override
    public void onFailSaved(int responseCode) {
        Toast.makeText(this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
    }

    //--------------------------------------------------------

    /***
     *  每一次完成insert，並更新當前進度條
     */
    @Override
    public void OnInsertedData() {
        progress++;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setProgress(progress);
                tvProgress.setText(String.format(getString(R.string.start_page_progress), progress, progressBar
                        .getMax()));
            }
        });
    }

    //--------------------------------------------------------

    /***
     * 當點選dialog中的同意 三秒後執行intent
     */
    @Override
    public void onPrivacyAccepted() {
        startActivity(new Intent(this, EntranceActivity.class));
        finish();
    }

    //--------------------------------------------------------

    /**
     * 設定更新dialog樣式
     *
     * @param dialog
     */
    private void setUpdateDialog(CustomDialog dialog) {

        dialog.setTitleBackgroundColor(ContextCompat.getColor(this, R.color.actionbar));
        dialog.setTitleTextColor(Color.WHITE);
        dialog.setDivider(ContextCompat.getColor(this, R.color.listview));
        dialog.setMessageBackgroundColor(ContextCompat.getColor(this, R.color.actionbar));
        dialog.setMessageTextSize(16);
        dialog.setMessageTextColor(Color.WHITE);
        dialog.setPositiveBackgroundColor(ContextCompat.getColor(this, R.color.actionbar));
        dialog.setPositiveTextColor(Color.WHITE);
        dialog.setPositiveTextSize(16);
    }

    //--------------------------------------------------------

    /***
     *  insert data
     */
    private class getDatas extends AsyncTask<Void, Void, Void> {

        private ArrayList<String[]> commodityCSV;
        private ArrayList<String[]> substanceCSV;
        private ArrayList<String[]> traditionCSV;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBar.setVisibility(View.VISIBLE);

            int total = 0;

            //不會將載入的CSV的第一筆資料(header)新增進DB，因此將筆數的最大值都須減少一筆
            if (!isLatestData(CSV_TRADITION)) {
                traditionCSV = csvParseHelper.parseCSV(CSVDownLoader.CSV_FILE_TRADITION);
                total = total + traditionCSV.size() - 1;
            }

            if (!isLatestData(CSV_SUBSTANCE)) {
                substanceCSV = csvParseHelper.parseCSV(CSVDownLoader.CSV_FILE_SUBSTANCE);
                total = total + substanceCSV.size() - 1;
            }

            if (!isLatestData(CSV_COMMODITY)) {
                commodityCSV = csvParseHelper.parseCSV(CSVDownLoader.CSV_FILE_COMMODITY);
                total = total + commodityCSV.size() - 1;
            }

            progressBar.setMax(total);
        }

        @Override
        protected Void doInBackground(Void... params) {
            isInsertData(traditionCSV, CSV_TRADITION);
            isInsertData(substanceCSV, CSV_SUBSTANCE);
            isInsertData(commodityCSV, CSV_COMMODITY);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            sharedPreferenceUtil.setDBVersion(csvData.getDbVersion());
            tvProgress.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            declarationDialog.show();
            handler.removeCallbacks(runnable);
            //已更新至最新版本
            Toast.makeText(StartPageActivity.this, getString(R.string.toast_updated), Toast.LENGTH_SHORT).show();
        }
    }

    //--------------------------------------------------------

    /***
     *  檢查csvVersion
     */
    private void checkCSVVersion(@CSVDrugType.DrugType String type) {

        String csvFile = "";
        switch (type) {
            //中藥藥品商品名
            case CSV_TRADITION:
                if (!isLatestData(CSV_TRADITION)) {
                    csvFile = CSVDownLoader.CSV_FILE_TRADITION;
                } else {
                    checkCSVVersion(CSV_SUBSTANCE);
                }
                break;
            //西藥禁用物質名
            case CSV_SUBSTANCE:
                if (!isLatestData(CSV_SUBSTANCE)) {
                    csvFile = CSVDownLoader.CSV_FILE_SUBSTANCE;
                } else {
                    checkCSVVersion(CSV_COMMODITY);
                }
                break;
            //西藥藥品商品名
            case CSV_COMMODITY:
                if (!isLatestData(CSV_COMMODITY)) {
                    csvFile = CSVDownLoader.CSV_FILE_COMMODITY;
                } else {
                    goToInsertData();
                }
                break;
        }

        //如果該csv有新版就進行下載
        if (!csvFile.equals("")) {
            csvDownLoader.downloadFileToInternalStorage(csvDataDropMap.get(type).getPath(), csvFile);
        }
    }

    //--------------------------------------------------------

    /**
     * 前往新增資料到DB
     */
    private void goToInsertData() {
        Message msg = new Message();
        msg.arg1 = HANDLER_MOTION_SUCCESS_CHECK_CSV;
        handler.sendMessage(msg);
    }

    //--------------------------------------------------------

    /**
     * 判斷藥品Table是否為最新的版本
     *
     * @param type
     * @return
     */
    private boolean isLatestData(@CSVDrugType.DrugType String type) {

        boolean isLatest = true;
        switch (type) {
            case CSV_TRADITION:
                isLatest = csvDataDropMap.get(CSV_TRADITION).getVersion() == sharedPreferenceUtil.getTraditionVersion();
                break;
            case CSV_SUBSTANCE:
                isLatest = csvDataDropMap.get(CSV_SUBSTANCE).getVersion() == sharedPreferenceUtil.getSubstanceVersion();
                break;
            case CSV_COMMODITY:
                isLatest = csvDataDropMap.get(CSV_COMMODITY).getVersion() == sharedPreferenceUtil.getCommodityVersion();
                break;
        }

        return isLatest;
    }


    //--------------------------------------------------------

    /**
     * 是否要新增資料，如需更新將會在更新完畢時設定SP裡所對應的csv之版本號
     *
     * @param data
     * @param type
     */
    private void isInsertData(ArrayList<String[]> data, @CSVDrugType.DrugType String type) {

        if (data != null) {
            switch (type) {
                case CSV_TRADITION:
                    dao.clearTable(TABLE_NAME_TRADITION);
                    dao.insertTradition(data);
                    sharedPreferenceUtil.setTraditionVersion(csvDataDropMap.get(CSV_TRADITION).getVersion());
                    break;
                case CSV_SUBSTANCE:
                    dao.clearTable(TABLE_NAME_SUBSTANCE);
                    dao.insertSubstance(data);
                    sharedPreferenceUtil.setSubstanceVersion(csvDataDropMap.get(CSV_SUBSTANCE).getVersion());
                    break;
                case CSV_COMMODITY:
                    dao.clearTable(TABLE_NAME_COMMODITY);
                    dao.insertCommodity(data);
                    sharedPreferenceUtil.setCommodityVersion(csvDataDropMap.get(CSV_COMMODITY).getVersion());
                    break;
            }
        }
    }

    //--------------------------------------------------------

    /**
     * 將得到的CSVData裡的藥物資料列表轉換成HashMap
     *
     * @param drugsBeans
     * @return
     */
    private HashMap<String, CSVData.DrugsBean> getCSVDataDropMap(ArrayList<CSVData.DrugsBean> drugsBeans) {

        HashMap<String, CSVData.DrugsBean> hashMap = new HashMap<>();

        for (CSVData.DrugsBean drugsBean : drugsBeans) {
            hashMap.put(drugsBean.getDrugType(), drugsBean);
        }

        return hashMap;
    }
}
