package tw.org.ssu.boxscore.illegaldrug.connect.csv.downloader;

import android.content.Context;
import android.support.annotation.StringDef;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by noel on 2018/5/4.
 */

public class CSVDownLoader {

    //存於內部資料夾的csv檔名稱 -西藥 藥品商品名
    public static final String CSV_FILE_COMMODITY = "commodity.csv";
    //存於內部資料夾的csv檔名稱 -西藥 禁用物質名
    public static final String CSV_FILE_SUBSTANCE = "substance.csv";
    //存於內部資料夾的csv檔名稱 -中藥 藥品商品名
    public static final String CSV_FILE_TRADITION = "tradition.csv";

    @StringDef({CSV_FILE_COMMODITY, CSV_FILE_SUBSTANCE, CSV_FILE_TRADITION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface CSVFileName {
    }

    private final int BUFFER_SIZE = 1024;

    private Context context;
    private Thread connectThread;
    private OnSavedInInternalStorageListener onSavedInInternalStorageListener;

    public CSVDownLoader(Context context) {
        this.context = context;
    }
    //-----------

    /***
     * 下載 並 存入內部資料夾
     * @param urlString 載點
     * @param fileName 欲存在內部資料夾中的檔案名稱（須包含副檔名）
     */
    public void downloadFileToInternalStorage(final String urlString, final String fileName) {

        connectThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
                    int responseCode = httpConn.getResponseCode();


                    //response code 正確的話
                    //取得串流 並 存入內部資料夾
                    if (responseCode == HttpURLConnection.HTTP_OK) {

                        /**
                         *  Log內部資料夾的路徑
                         *  Log.e("a", context.getFilesDir().getAbsolutePath());
                         *
                         *  Log內部資料夾的項目總數
                         *  Log.e("a", context.fileList().length + "");
                         *  Log將他們的名稱一個一個印出來
                         *  for (int i = 0; i < context.fileList().length; i++) {
                         *  Log.e("packageName", context.fileList()[i]);
                         *  }
                         * */


                        /***
                         *  用來Log此內部路徑中所有檔案名稱
                         *  File[] fileList = new File(context.getFilesDir().getAbsolutePath(), packageName).listFiles();
                         *  CharSequence[] list = new CharSequence[fileList.length];
                         *  for (int i = 0; i < list.length; i++) {
                         *  list[i] = fileList[i].getName();
                         *  Log.e(i + "=", list[i].toString());
                         *  }
                         */


                        InputStream inputStream = httpConn.getInputStream();

                        File internalFile = new File(context.getFilesDir().getAbsolutePath(), fileName);
                        FileWriter writer = new FileWriter(internalFile);
                        writer.append(convertStreamToString(new InputStreamReader(inputStream, "UTF-8")));
                        if (onSavedInInternalStorageListener != null) {
                            //當成供下載並儲存於內部資料夾
                            onSavedInInternalStorageListener.onSuccessSaved(responseCode, internalFile.getAbsolutePath(), fileName);
                        }
                        writer.flush();
                        writer.close();
                        inputStream.close();
                    }
                    //當無法下載
                    else {
                        if (onSavedInInternalStorageListener != null) {
                            onSavedInInternalStorageListener.onFailSaved(responseCode);
                        }
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        connectThread.start();
    }

    //----------


    /***
     * 將server端回傳 一行一行加入StringBuilder並換行 直到沒有
     * @param inputStreamReader
     * @return
     */
    private String convertStreamToString(InputStreamReader inputStreamReader) {
        BufferedReader reader = new BufferedReader(inputStreamReader, BUFFER_SIZE);
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
            reader.close();
            inputStreamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    //------------

    /***
     *  接獲資料後 的接口
     * @param onSavedInInternalStorageListener
     */
    public void setOnSavedInInternalStorageListener(OnSavedInInternalStorageListener onSavedInInternalStorageListener) {
        this.onSavedInInternalStorageListener = onSavedInInternalStorageListener;
    }
}
