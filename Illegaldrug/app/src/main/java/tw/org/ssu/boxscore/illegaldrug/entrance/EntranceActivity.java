package tw.org.ssu.boxscore.illegaldrug.entrance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tw.org.ssu.boxscore.illegaldrug.BuildConfig;
import tw.org.ssu.boxscore.illegaldrug.MainActivity;
import tw.org.ssu.boxscore.illegaldrug.R;
import tw.org.ssu.boxscore.illegaldrug.about.AboutActivity;
import tw.org.ssu.boxscore.illegaldrug.contact.ContactActivity;
import tw.org.ssu.boxscore.illegaldrug.satisfaction.SatisfactionActivity;


public class EntranceActivity extends FragmentActivity {

    @BindView(R.id.back)
    FrameLayout back;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.tv_commodity)
    TextView tvCommodity;
    @BindView(R.id.tv_substance)
    TextView tvSubstance;
    @BindView(R.id.tv_tradition)
    TextView tvTradition;
    @BindView(R.id.tv_about)
    TextView tvAbout;
    @BindView(R.id.tv_contact)
    TextView tvContact;
    @BindView(R.id.tv_satisfaction)
    TextView tvSatisfaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrance);
        ButterKnife.bind(this);

        back.setVisibility(View.GONE);

        tvCommodity.setText(getSpannedText(getString(R.string.entrance_title_commodity), R.color.actionbar, R.color.tab_clicked_subtitle));
        tvSubstance.setText(getSpannedText(getString(R.string.entrance_title_substance), R.color.actionbar, R.color.tab_clicked_subtitle));
        tvTradition.setText(getSpannedText(getString(R.string.entrance_title_tradition), R.color.actionbar, R.color.tab_clicked_subtitle));
        tvAbout.setText(getSpannedText(getString(R.string.entrance_title_about), R.color.actionbar, R.color.tab_clicked_subtitle));
        tvContact.setText(getSpannedText(getString(R.string.entrance_title_contact), R.color.actionbar, R.color.tab_clicked_subtitle));
        tvSatisfaction.setText(getSpannedText(getString(R.string.entrance_title_satisfaction), R.color.actionbar, R.color.tab_clicked_subtitle));
        tvVersion.setText(String.format(getString(R.string.entrance_version), BuildConfig.VERSION_NAME));

    }


    //-----------------------

    @OnClick({R.id.tv_substance, R.id.tv_commodity, R.id.tv_tradition, R.id.tv_about, R.id.tv_contact, R.id.tv_satisfaction})
    public void onViewClicked(View view) {
        Intent it = new Intent();
        switch (view.getId()) {
            //西藥藥品商品名
            case R.id.tv_commodity:
                it.putExtra("tab", MainActivity.TAB_COMMODITY);
                it.setClass(this, MainActivity.class);
                break;
            //西藥禁用物質名
            case R.id.tv_substance:
                it.putExtra("tab", MainActivity.TAB_SUBSTANCE);
                it.setClass(this, MainActivity.class);
                break;
            //中藥藥品商品名
            case R.id.tv_tradition:
                it.putExtra("tab", MainActivity.TAB_TRADITION);
                it.setClass(this, MainActivity.class);
                break;
            //關於系統
            case R.id.tv_about:
                it.setClass(this, AboutActivity.class);
                break;
            //聯絡我們
            case R.id.tv_contact:
                it.setClass(this, ContactActivity.class);
                break;
            //使用者滿意度調查
            case R.id.tv_satisfaction:
                it.setClass(this, SatisfactionActivity.class);
                break;
        }
        startActivity(it);
    }


    //----------------------

    /***
     * 調整部分文字顏色 或大小
     * @param tabName
     * @return
     */
    private CharSequence getSpannedText(String tabName, int titleColor, int contentColor) {

        SpannableStringBuilder builder = new SpannableStringBuilder(tabName);

        int start = tabName.indexOf("-");
        int end = tabName.lastIndexOf("-") + 1;

        builder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, titleColor)), 0, start, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new RelativeSizeSpan(1.08f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, contentColor)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return builder;
    }


}
